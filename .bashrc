#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source $HOME/.variables
source $HOME/.bash_aliases

if [ "$DISTRO" = "Arch" ]; then
	source /usr/share/git/completion/git-prompt.sh
fi

shopt -s autocd

right_prompt_1()
{
    printf "%*s" $COLUMNS "$(__git_ps1 ' (%s)')"
}

right_prompt_2()
{
    printf "%*s" $COLUMNS $EXIT
}

draw_line()
{
    local COLUMNS="$COLUMNS"
    while ((COLUMNS-- > 0)); do
        #printf '\u2500'
        printf '='
    done
}

__prompt_command() {
    local EXIT="$?"             # This needs to be first
    # 1
    PS1="\[\033[01;33m\]$(draw_line)\n"
    PS1+="\[\033[01;93m\]\[$(tput sc; right_prompt_1)\]\r\[\033[01;93m\]┌─\[\033[01;92m\][\[\033[01;94m\]\w\[\033[01;92m\]]\n"
    # 2
    PS1+="\[$(tput sc; right_prompt_2)\]\r\[\033[01;93m\]│\n"
    # 3
    PS1+="\[\033[01;93m\]└─────$([[ $EXIT -eq 0 ]] && echo "➤" || echo "\[\e[38;5;124m\]➤") \[\033[00m\]"
}

PROMPT_COMMAND=__prompt_command

pokemon-colorscripts -r 1-3
