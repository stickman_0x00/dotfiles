# Dotfile

# Restore

```bash
# Set alias
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# clone bare repository
git clone --bare https://gitlab.com/stickman_0x00/dotfiles $HOME/.dotfiles


# check problems
config checkout
# Remove files
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} rm {}
# recheck if any problems left
config checkout

# set flag
config config --local status.showUntrackedFiles no

# restore
config pull
```

# VS Code

## Update VS Code extensions file

```bash
code --list-extensions > code --list-extensions > .config/dotfiles/vscode_extensions.txt
```

## Install extensions from file

```bash
cat .config/dotfiles/vscode_extensions.txt |  xargs -n 1 code --install-extension
```
