if [ "$DISTRO" = "Debian" ]; then
	# ls
	alias ls='exa --icons --color=always --group'
	
	# colors
	alias cat='batcat --style=plain'

	alias update='sudo apt update && sudo apt upgrade; flatpak update; fwupdUpdate'
elif [ "$DISTRO" = "Arch" ]; then
	# ls
	alias ls='eza --icons --color=always --group'
	
	# colors
	alias cat='bat --style=plain'

	alias update='yay; flatpak update'
fi

alias l='ls'
alias ll='ls -alh'

# Colors
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'

man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
#__git_complete config __git_main

# debian
alias update='yay; flatpak update'

# arch linux
alias remove='sudo pacman -Rnsc'
alias autoremove='remove $(pacman -Qdt | cut -f 1 -d " ")'

# folders
alias -- -='cd -'

# archives and compression
alias untar='tar -zxvf'

# network
alias ports='ss -tnlp'
alias testPort='nmap -Pn -sV -A -p'
alias myip="ip -f inet address | grep inet | grep -v 'lo$' | cut -d ' ' -f 6,13 && curl ifconfig.me && echo ' external ip'"

cheat() {
	curl cheat.sh/$1
}

alias historyclean='cat /dev/null > ~/.bash_history && history -c && exit'

# GO
alias godoc='godoc --http=:6060'
