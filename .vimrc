" Colorscheme
syntax on 
set t_Co=256

" Line number
set number
"set relative number

set showcmd " show typed command being typed

set cursorline
set mouse=v

set nohlsearch " highlighting off
set ignorecase " Search case insensitive..."

set splitright " set default split to right
set splitbelow

" Tabs
set ts=4 "tab number
set shiftwidth=4
set tabstop=4
set autoindent
	

" Nerd Tree
" absolute width of netrw window
let g:netrw_winsize = 75
" tree-view
let g:netrw_liststyle = 3
" sort is affecting only: directories on the top, files below
let g:netrw_sort_sequence = '[\/]$,*'
" open file to the right
"let g:netrw_browse_split = 2
	
" Spell check
"set spell

" This enables us to undo files even if you exit Vim.
set undofile
set undodir=~/.vim/undodir

" Custom commands
" Save file if forgot sudo
cmap w!! w !sudo tee > /dev/null %

" Plugins
filetype plugin on
